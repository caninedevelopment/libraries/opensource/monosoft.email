namespace Monosoft.Email.Application.Resources.Email.Commands.Send
{
    using Monosoft.Common.Command.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Text.RegularExpressions;

    public class Request : IDtoRequest
    {
        public void Validate()
        {
            bool IsValidEmail(string strIn)
            {
                // Return true if strIn is in valid e-mail format.
                return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            }

            if (string.IsNullOrWhiteSpace(NameOfSender))
            {
                throw new Monosoft.Common.Exceptions.ValidationException(nameof(NameOfSender));
            }

            if (string.IsNullOrWhiteSpace(Sender) || !IsValidEmail(Sender))
            {
                throw new Monosoft.Common.Exceptions.ValidationException(nameof(Sender));
            }

            if (string.IsNullOrWhiteSpace(Title))
            {
                throw new Monosoft.Common.Exceptions.ValidationException(nameof(Title));
            }

            if (Receivers == null)
            {
                throw new ValidationException(nameof(Receivers));
            }

            if (Receivers.Count == 0)
            {
                throw new ValidationException(nameof(Receivers));
            }

            for (int i = 0; i < Receivers.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(Receivers[i]) || !IsValidEmail(Receivers[i]))
                {
                    throw new Monosoft.Common.Exceptions.ValidationException(nameof(Receivers));
                }
            }

            if (string.IsNullOrWhiteSpace(PlainMessage) && string.IsNullOrWhiteSpace(HtmlMessage))
            {
                throw new Monosoft.Common.Exceptions.ValidationException("Both message is empty");
            }

            if (Ccs != null)
            {
                for (int i = 0; i < Ccs.Count; i++)
                {
                    if (string.IsNullOrWhiteSpace(Ccs[i]) || !IsValidEmail(Ccs[i]))
                    {
                        throw new Monosoft.Common.Exceptions.ValidationException(nameof(Ccs));
                    }
                }
            }

            if (Bccs != null)
            {
                for (int i = 0; i < Bccs.Count; i++)
                {
                    if (string.IsNullOrWhiteSpace(Bccs[i]) || !IsValidEmail(Bccs[i]))
                    {
                        throw new Monosoft.Common.Exceptions.ValidationException(nameof(Bccs));
                    }
                }
            }

        }

        public string NameOfSender { get; set; }
        public string Sender { get; set; }
        public string Title { get; set; }
        public List<string> Receivers { get; set; }
        public string PlainMessage { get; set; }
        public string HtmlMessage { get; set; }
        public List<string> Ccs { get; set; }
        public List<string> Bccs { get; set; }
    }

}