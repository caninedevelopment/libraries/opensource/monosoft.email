namespace Monosoft.Email.Application.Resources.Email.Commands.Send
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Email.Domain.Entities;
    using Monosoft.Email.Domain.Interfaces;

    public class Command : IProcedure<Request>
    {
        private readonly IEmailSend eamilSend;

        public Command(IEmailSend eamilSend)
        {
            this.eamilSend = eamilSend;
        }

        public void Execute(Request input)
        {
            var email = new EmailNotification();
            {
                email.NameOfSender = input.NameOfSender;
                email.Sender = input.Sender;
                email.Title = input.Title;
                email.Receivers = input.Receivers;
                email.PlainMessage = input.PlainMessage;
                email.HtmlMessage = input.HtmlMessage;
                email.Ccs = input.Ccs;
                email.Bccs = input.Bccs;
            }

            eamilSend.SendEmail(email);
        }
    }
}