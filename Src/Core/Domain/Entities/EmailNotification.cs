﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Email.Domain.Entities
{
    public class EmailNotification
    {
        public string NameOfSender { get; set; }
        public string Sender { get; set; }
        public string Title { get; set; }
        public List<string> Receivers { get; set; }
        public string PlainMessage { get; set; }
        public string HtmlMessage { get; set; }
        public List<string> Ccs { get; set; }
        public List<string> Bccs { get; set; }
    }
}
