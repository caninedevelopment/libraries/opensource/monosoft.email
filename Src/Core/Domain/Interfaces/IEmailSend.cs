﻿using Monosoft.Email.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Monosoft.Email.Domain.Interfaces
{
    public interface IEmailSend
    {
        void SendEmail(EmailNotification email);
    }
}
