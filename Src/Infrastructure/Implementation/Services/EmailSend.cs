﻿using MailKit.Net.Smtp;
using MimeKit;
using Monosoft.Email.Domain.Entities;
using Monosoft.Email.Domain.Interfaces;
using Polly;
using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Monosoft.Email.Implementation.Services
{
    public class EmailSend : IEmailSend
    {
        public class Smtpsetting
        {
            public string SmtpServerName { get; set; }
            public string SmtpUserName { get; set; }
            public string SmtpPassword { get; set; }
            public int SmtpPort { get; set; }

        }

        private readonly Smtpsetting smtpsetting;
        private SmtpClient client = new SmtpClient();
        public EmailSend(Smtpsetting smtpsetting)
        {
            this.smtpsetting = smtpsetting;
        }

        ~EmailSend()
        {
            if (client.IsConnected == true)
                client.Disconnect(true);
        }
        RetryPolicy retryPolicy = Policy.Handle<Exception>()
           .WaitAndRetry(10, retryAttempt => TimeSpan.FromMinutes(1)); 

        public void SendEmail(EmailNotification email)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(email.NameOfSender, email.Sender));
            foreach (var receiver in email.Receivers)
            {
                message.To.Add(MailboxAddress.Parse(receiver));
            }

            message.Subject = email.Title;
            if (email.Ccs != null)
            {
                if (email.Ccs.Count > 0)
                {
                    foreach (var cc in email.Ccs)
                    {
                        message.Cc.Add(MailboxAddress.Parse(cc));
                    }
                }
            }

            if (email.Bccs != null)
            {
                if (email.Bccs.Count > 0)
                {
                    foreach (var bcc in email.Bccs)
                    {
                        message.Bcc.Add(MailboxAddress.Parse(bcc));
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(email.PlainMessage) && !string.IsNullOrWhiteSpace(email.HtmlMessage))
            {
                var alternative = new Multipart("alternative");
                alternative.Add(new TextPart("plain") { Text = email.PlainMessage });
                alternative.Add(new TextPart("html") { Text = email.HtmlMessage });

                var multipart = new Multipart("mixed");
                multipart.Add(alternative);

                message.Body = multipart;
            }

            if (!string.IsNullOrWhiteSpace(email.PlainMessage) && string.IsNullOrWhiteSpace(email.HtmlMessage))
            {
                message.Body = new TextPart("plain")
                {
                    Text = email.PlainMessage,
                };
            }

            if (!string.IsNullOrWhiteSpace(email.HtmlMessage) && string.IsNullOrWhiteSpace(email.PlainMessage))
            {
                message.Body = new TextPart("html")
                {
                    Text = email.HtmlMessage,
                };
            }


            retryPolicy.Execute(() =>
                {
                    if (client.IsConnected == false)
                    {
                        client.Connect(smtpsetting.SmtpServerName, smtpsetting.SmtpPort);
                        client.Authenticate(smtpsetting.SmtpUserName, smtpsetting.SmtpPassword);
                    }
                    client.Send(message);
                }
            );
        }
    }
}