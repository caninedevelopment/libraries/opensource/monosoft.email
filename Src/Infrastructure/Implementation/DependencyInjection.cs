namespace Infrastructure.Implementation
{
    using Microsoft.Extensions.DependencyInjection;
    using Monosoft.Email.Domain.Interfaces;
    using Monosoft.Email.Implementation.Services;

    public static class DependencyInjection
    {
        public static IServiceCollection AddImplementation(this IServiceCollection services, string smtpServerName, string smtpUserName, string smtpPassword, int smtpPort)
        {
            var smtpsetting = new Monosoft.Email.Implementation.Services.EmailSend.Smtpsetting
            {
                SmtpPassword = smtpPassword,
                SmtpServerName = smtpServerName,
                SmtpUserName = smtpUserName,
                SmtpPort = smtpPort
            };

            services.AddSingleton<IEmailSend>(x => new EmailSend(smtpsetting));
            return services;
        }
    }
}